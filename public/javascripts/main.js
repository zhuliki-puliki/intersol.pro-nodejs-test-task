var testTask = {};

testTask.functions = {};

testTask.functions.getXmlHttp = function(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && (typeof(XMLHttpRequest) != 'undefined')) {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

testTask.functions.requestToServer = function(cmd, query, methods) {
  var xmlhttp = testTask.functions.getXmlHttp();
  xmlhttp.open('POST', document.URL, true);
  xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xmlhttp.onreadystatechange = function(){
    if (xmlhttp.readyState == 4) {
      if ((typeof(methods['before']) == 'function') && !methods['before']()) {
        return;
      }
      if (xmlhttp.status == 200) {
        var answer = xmlhttp.responseText.replace(/^\s+/i, '').replace(/\s+$/i, '');
        try {
          answer = JSON.parse(answer);
        } catch (e) {
          console.log(e);
          console.log(answer);
          return;
        }
        if (answer.error) {
          console.log('ERROR =================');
          console.log(answer.result);
          console.log('=======================');
        }
        if ((typeof(methods['success']) == 'function') && !methods['success'](answer)) {
          return;
        }
      } else if ((typeof(methods['error']) == 'function') && !methods['error']()) {
        return;
      }
      if (typeof(methods['after']) == 'function') {
        methods['after']();
      }
    }
  };
  xmlhttp.send(
    'cmd=' + encodeURIComponent(cmd) +
    '&query=' + encodeURIComponent(JSON.stringify(query))
  );
}

testTask.vars = {};