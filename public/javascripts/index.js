testTask.vars.widgets = {
  volume: 0,
  isLoading: false,
  isCreating: false
};

testTask.functions.createWidget = function(widget) {
  var widgetList = document.getElementById('city-weather-editors');
  widgetList.classList.remove('hidden');
  var widgetTag = document.createElement('tr');
  widgetTag.classList.add('weather-editors-data');
  widgetTag.setAttribute('data-id', widget.id);
  widgetTag.innerHTML =
    '<td class="city">' + 
      document.querySelector('#city-select option[value="' + widget.city + '"]').text +
    '</td>' +
    '<td class="period">' + widget.period + '</td>' +
    '<td class="direction">' +
      (widget.direction < 1 ? 'горизонтальное' : 'вертикальное') +
    '</td>' +
    '<td class="buttons">' +
      '<div class="button">' +
        '<span>Получить код</span>' +
      '</div>' +
      '<div class="button">' +
        '<span>Удалить</span>' +
      '</div>' +
    '</td>';
  widgetTag.children[0].onclick = widgetTag.children[1].onclick = widgetTag.children[2].onclick = function(){
    if (widgetTag.classList.contains('removed')) {
      return false;
    }
    var editWidgets = widgetTag.parentNode.getElementsByClassName('edit');
    for (var i = 0; i < editWidgets.length; ++i) {
      editWidgets[i].classList.remove('edit');
    }
    this.parentNode.classList.add('edit');
    var creating = document.getElementById('weather-editor-title');
    creating.children[0].classList.add('hidden');
    creating.children[1].classList.remove('hidden');
    document.getElementById('weather-editor-new').classList.remove('hidden');
    document.getElementById('weather-editor-parameters').setAttribute('data-id', widget.id);
    document.querySelector('#weather-editor-data td.city select option[value="' + widget.city + '"]').selected = true;
    document.querySelector('#weather-editor-data td.period input').value = widget.period;
    document.querySelectorAll('#weather-editor-data td.direction label')[widget.direction].click();
    return true;
  };
  var buttons = widgetTag.children[3].getElementsByClassName('button');
  buttons[0].onclick = function(){
    if (widgetTag.classList.contains('removed')) {
      return false;
    }
    document.getElementById('city-weather-editors').classList.add('hidden');
    document.getElementById('city-weather-code').classList.remove('hidden');
    document.getElementById('weather-code-value').innerHTML =
      "&lt;script type='text/javascript'&gt;" +
        "var weatherWidgetInit = function(url) {" +
          "var _iframe = document.createElement('iframe');" +
          (
            parseInt(widget.direction) < 1 ?
              "_iframe.height = 165;" +
              "_iframe.width = 735;" :
              "_iframe.height = 430;" +
              "_iframe.width = 224;"
          ) +
          "_iframe.src = url;" +
          "_iframe.style['border'] = '0px';" +
          "_iframe.style['position'] = 'absolute';" +
          "_iframe.style['z-index'] = 5000;" +
          "_iframe.style['overflow'] = 'hidden';" +
          "document.currentScript.parentNode.insertBefore(_iframe, document.currentScript);" +
        "};" +
        "weatherWidgetInit('" + document.URL.match(/^[^\/]+\/\/[^\/]+/)[0] + "/widget/" + widget.id + "');" +
      "&lt;/script&gt;";
  };
  buttons[1].onclick = function(){
    widgetTag.classList.add('removed');
    testTask.functions.requestToServer('remove', {id: widget.id}, {
      success: function(answer){
        if (answer.error) {
          return true;
        }
        widgetTag.parentNode.removeChild(widgetTag);
        if (widgetList.querySelector('tbody').children.length < 1) {
          widgetList.classList.add('hidden');
        }
        return true;
      }
    });
  };
  var oldChild = widgetList.querySelector('tbody tr[data-id="' + widget.id + '"]');
  if (oldChild == null) {
    widgetList.querySelector('tbody').appendChild(widgetTag);
  } else {
    widgetTag.classList.add('edit');
    widgetList.querySelector('tbody').replaceChild(widgetTag, oldChild);
  }
}

testTask.functions.loadReadyWidgets = function() {
  if (testTask.vars.widgets.isLoading) {
    return false;
  }
  testTask.vars.widgets.isLoading = true;
  var lastPosition = document.querySelectorAll('#city-weather-editors tbody tr').length;
  testTask.functions.requestToServer('loadwidgets', {lastPosition: lastPosition, count: 15}, {
    before: function(){
      testTask.vars.widgets.isLoading = false;
      return true;
    },
    success: function(answer){
      if (answer.error) {
        return true;
      }
      testTask.vars.widgets.volume = answer.result.volume;
      for (var i = 0; i < answer.result.units.length; ++i) {
        testTask.functions.createWidget(answer.result.units[i]);
        --testTask.vars.widgets.volume;
      }
      return true;
    }
  });
  return true;
}

window.addEventListener('load', function() {
  var directions = document.querySelectorAll('tr#weather-editor-data td.direction label');
  var directionChanging = function() {
    for (var i = 0; i < directions.length; ++i) {
      if (directions[i].getElementsByTagName('input')[0].checked) {
        directions[i].classList.add('checked');
      } else {
        directions[i].classList.remove('checked');
      }
    }
  };
  for (var i = 0; i < directions.length; ++i) {
    directions[i].getElementsByTagName('input')[0].onchange = function() {
      directionChanging();
    };
  }

  document.getElementById('weather-editor-new').onclick = function() {
    var editWidgets = document.querySelectorAll('#city-weather-editors tbody > tr.edit');
    for (var i = 0; i < editWidgets.length; ++i) {
      editWidgets[i].classList.remove('edit');
    }
    var creating = document.getElementById('weather-editor-title');
    creating.children[0].classList.remove('hidden');
    creating.children[1].classList.add('hidden');
    this.classList.add('hidden');
    document.getElementById('weather-editor-parameters').setAttribute('data-id', 0);
    document.querySelector('#weather-editor-data td.city select option:first-child').selected = true;
    document.querySelector('#weather-editor-data td.period input').value = 1;
    document.querySelector('#weather-editor-data td.direction label').click();
  };

  document.getElementById('weather-editor-send').onclick = function() {
    if (testTask.vars.widgets.isCreating) {
      return false;
    }
    testTask.vars.widgets.isCreating = true;
    var query = {};
    query.id = parseInt(document.getElementById('weather-editor-parameters').getAttribute('data-id'));
    query.city = parseInt(document.querySelector('#weather-editor-data td.city select').value);
    query.period = parseInt(document.querySelector('#weather-editor-data td.period input').value);
    if (query.period > 7) {
      query.period = 7;
    } else if (query.period < 1) {
      query.period = 1;
    }
    query.direction = parseInt(document.querySelector('#weather-editor-data td.direction input:checked').value);
    var creating = document.getElementById('weather-editor-title');
    testTask.functions.requestToServer('create', query, {
      before: function() {
        testTask.vars.widgets.isCreating = false;
        return true;
      },
      success: function(answer) {
        if (answer.error) {
          return true;
        }
        if (testTask.vars.widgets.isLoading || (testTask.vars.widgets.volume > 0)) {
          ++testTask.vars.widgets.volume;
        } else {
          testTask.functions.createWidget(answer.result);
        }
        return true;
      }
    });
  };

  document.getElementById('city-weather-code-close').onclick = function() {
    document.getElementById('city-weather-editors').classList.remove('hidden');
    document.getElementById('city-weather-code').classList.add('hidden');
  };

  document.getElementById('city-weather-editors').onscroll = function() {
    if (
      !testTask.vars.widgets.isLoading &&
      (testTask.vars.widgets.volume > 0) &&
      (this.scrollTop + this.clientHeight >= this.scrollHeight - 10)
    ) {
      testTask.functions.loadReadyWidgets();
    }
  };

  testTask.functions.loadReadyWidgets();
});