window.addEventListener('load', function(){
  var setWeatherDay = function(day){
    if (!(testTask.weather instanceof Array)) {
      return false;
    }
    document.querySelector('#weather-precipIntensity span.value').innerHTML = 
      (testTask.weather[day].precipIntensity * 100) + '%';
    document.querySelector('#weather-precipprobability span.value').innerHTML = 
      (testTask.weather[day].precipProbability * 100) + '%';
    document.querySelector('#weather-temperature span.value').innerHTML = 
      testTask.weather[day].temperatureMin + '/' + testTask.weather[day].temperatureMax + '&deg;C';
    document.querySelector('#weather-humidity span.value').innerHTML = 
      (testTask.weather[day].humidity * 100) + '%';
    document.querySelector('#weather-windSpeed span.value').innerHTML = 
      testTask.weather[day].windSpeed + ' м/с';
    document.querySelector('#weather-cloudCover span.value').innerHTML = 
      (testTask.weather[day].cloudCover * 100) + '%';
    document.querySelector('#weather-pressure span.value').innerHTML = 
      testTask.weather[day].pressure + ' мм рт.ст.';
  };

  var daySelector = document.querySelector('#widget-setting select');
  daySelector.onchange = function(){
    setWeatherDay(this.value);
  };

  setWeatherDay(0);

  var updateWeatherData = function(){
    testTask.functions.requestToServer('update', [], {
      success: function(answer) {
        if (answer.error) {
          return true;
        }
        // console.log((new Date()) + ': update');
        testTask.weather = answer.result;
        var oldChoice = daySelector.selectedOptions[0].text, selectedValue = -1;
        daySelector.innerHTML = '';
        for (var i = 0; i < testTask.setting.period; ++i) {
          if (i < testTask.weather.length) {
            var
              someDate = new Date(testTask.weather[i].time * 1000),
              month = someDate.getMonth() + 1, day = someDate.getDate(),
              dayValue = someDate.getFullYear() + '.' + ((month < 10 ? '0' : '') + month) + '.' + ((day < 10 ? '0' : '') + day);
            daySelector.innerHTML += '<option value="' + i + '">' + dayValue + '</option>';
            if ((selectedValue < 0) || (oldChoice == dayValue)) {
              selectedValue = i;
            }
          }
        }
        if (selectedValue >= 0) {
          daySelector.querySelector('option[value="' + selectedValue + '"]').selected = true;
          setWeatherDay(selectedValue);
        }
        setTimeout(updateWeatherData, 60000);
        return true;
      }
    });
  };

  setTimeout(updateWeatherData, 60000);
});