var 
  express = require('express'),
  router = express.Router(),
  redis = require("redis"),
  client = redis.createClient();

client.on('error', function(err) {
  console.log('REDIS ERROR: ' + err);
});

var cities = [
  {id: 1, name: 'Москва'},
  {id: 2, name: 'Санк-Петербург'},
  {id: 3, name: 'Нижний Новгород'}
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {windowTitle: "City's weather forecast", cities: cities });
});

var checkQuery = function(keys, types, query, badKeys) {
  var coolTypes = [];
  if (typeof(types) == 'string') {
    coolTypes.push(types);
  } else if (types instanceof Array) {
    coolTypes = types;
  }
  var data = [], typeNum = coolTypes.length > 0 ? 0 : -1;
  for (var i = 0; i < keys.length; ++i) {
    if ((typeNum > -1) && (typeof(query[keys[i]]) != coolTypes[typeNum])) {
      return {error: true, result: [keys[i], coolTypes[typeNum]]};
    } else {
      if (badKeys.indexOf(keys[i]) < 0) {
        data.push(keys[i], query[keys[i]]);
      }
      if (typeNum > -1) {
        if (typeNum == coolTypes.length - 1) {
          typeNum = 0;
        } else {
          ++typeNum;
        }
      }
    }
  }
  return {error: false, result: data};
};

router.post('/', function(req, res, next) {
  if ((typeof(req.body.cmd) != 'string') || (typeof(req.body.query) != 'string')) {
    res.send(JSON.stringify({error: true, result: 'No parameters `cmd` and `query`'}));
    return false;
  }
  var query = req.body.query;
  try {
    query = JSON.parse(query);
  } catch (e) {
    res.send(JSON.stringify({error: true, result: 'Parameter `query` is not JSON'}));
    return false;
  }
  if (req.body.cmd == 'create') {
    var checking = checkQuery(['id', 'city', 'period', 'direction'], 'number', query, ['id']);
    if (checking.error) {
      res.send(JSON.stringify({error: true, result: 'Field `' + checking.result[0] + '` must has number type'}));
      return false;
    }
    var createWidget = function(unitName){
      client.hmset(unitName, checking.result, function(error, result) {
        if (error == null) {
          res.send(JSON.stringify({error: false, result: query}));
        } else {
          res.send(JSON.stringify({error: true, result: error}));
        }
      });
    };
    if (query.id > 0) {
      client.exists('testtask:widgets:unit:' + query.id, function(error, result) {
        if (error != null) {
          res.send(JSON.stringify({error: true, result: error}));
        } else if (parseInt(result) < 1) {
          res.send(JSON.stringify({error: true, result: 'Widget with id equal ' + query.id + ' not exists'}));
        } else {
          createWidget('testtask:widgets:unit:' + query.id);
        }
      });
    } else {
      client.incr('testtask:widgets:maxid', function(error, result) {
        if (error == null) {
          query.id = result;
          checking.result.splice(0, 0, 'id', result);
          createWidget('testtask:widgets:unit:' + result);
        } else {
          res.send(JSON.stringify({error: true, result: error}));
        }
      });
    }
  } else if (req.body.cmd == 'loadwidgets') {
    var checking = checkQuery(['lastPosition', 'count'], 'number', query, []);
    if (checking.error) {
      res.send(JSON.stringify({error: true, result: 'Field `' + checking.result[0] + '` must has number type'}));
      return false;
    }
    client.keys('testtask:widgets:unit:*', function(error, result){
      if (error != null) {
        res.send(JSON.stringify({error: true, result: error}));
        return false;
      }
      var units = [];
      var loadUnit = function(position) {
        if (position == result.length) {
          res.send(JSON.stringify({
            error: false,
            result: {
              volume: position - query.lastPosition,
              units: units.slice(query.lastPosition, query.lastPosition + query.count)
            }
          }));
          return true;
        }
        client.hgetall(result[position], function(error, result){
          if (error == null) {
            result.id = parseInt(result.id);
            var j = 0;
            for (; (j < units.length) && (result.id > units[j].id); ++j);
            units.splice(j, 0, result);
            loadUnit(position + 1);
          } else {
            res.send(JSON.stringify({error: true, result: error}));
          }
        });
        return true;
      };
      loadUnit(0);
    });
  } else if (req.body.cmd == 'remove') {
    if (typeof(query.id) != 'number') {
      res.send(JSON.stringify({error: true, result: 'Field `id` must has number type'}));
      return false;
    }
    client.del('testtask:widgets:unit:' + query.id, function(error, result){
      if (error == null) {
        res.send(JSON.stringify({error: false, result: 'ok'}));
      } else {
        res.send(JSON.stringify({error: true, result: error}));
      }
    });
  } else {
    res.send(JSON.stringify({error: true, result: 'Unknown command: ' + req.body.cmd}));
    return false;
  }
});

module.exports = router;
