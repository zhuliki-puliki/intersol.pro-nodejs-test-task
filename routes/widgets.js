var 
  express = require('express'),
  router = express.Router(),
  redis = require("redis"),
  client = redis.createClient();

client.on('error', function(err) {
  console.log('REDIS ERROR: ' + err);
});

var cities = [
  {name: 'Москва', coordinates: [55.7522222, 37.6155556]},
  // {name: 'Москва', coordinates: [55.755826, 37.617300]},
  {name: 'Санкт-Петербург', coordinates: [59.8944444, 30.2641667]},
  // {name: 'Санкт-Петербург', coordinates: [59.934280, 30.335099]},
  {name: 'Нижний Новгород', coordinates: [56.296504, 43.936059]}
];

var Forecast = require('forecast');

// Initialize 
var forecast = new Forecast({
  service: 'darksky',
  key: '5ff42578e9d9dbd4eb97b7bc83cc8420', //'your-api-key',
  units: 'celcius',
  cache: true,      // Cache API requests 
  ttl: {            // How long to cache requests. Uses syntax from moment.js: http://momentjs.com/docs/#/durations/creating/ 
    minutes: 27,
    seconds: 45
  }
});

var loadWeatherData = function(cityId, callback){
  if (!/^\d+$/.test(cityId)) {
    callback(true, {message: 'No widget with id equal ' + cityId});
    return;
  }
  client.exists('testtask:widgets:unit:' + cityId, function(error, result){
    if (error) {
      callback(true, {message: 'Problem with checking of widget', error: error});
    } else if (result < 1) {
      callback(true, {message: 'No widget with id equal ' + cityId});
    } else {
      client.hgetall('testtask:widgets:unit:' + cityId, function(error, result){
        if (error) {
          callback(true, {message: 'Problem with data getting of widget', error: error});
        } else {
          forecast.get(cities[result.city - 1].coordinates, function(error, weather){
            if (error) {
              callback(true, {message: 'Problem with weather data', error: error});
            } else {
              callback(false, {widget: result, weather: weather});
            }
          });
        }
      });
    }
  });  
};

router.get('/:id', function(req, res, next) {
  loadWeatherData(req.params.id, function(error, result) {
    if (error) {
      res.render('error', {windowTitle: 'You are very bad', message: result.message, error: result.error});
    } else {
      res.render('widget', {
        windowTitle: 'Weather widget',
        city: {
          name: cities[result.widget.city - 1].name,
          weather: result.weather.daily.data,
          setting: result.widget
        }
      });
    }
  });
});

router.post('/:id', function(req, res, next) {
  loadWeatherData(req.params.id, function(error, result) {
    if (error) {
      res.send(JSON.stringify({error: true, result: result}));
    } else {
      res.send(JSON.stringify({error: false, result: result.weather.daily.data}));
    }
  });
});

module.exports = router;